
#Keshav Patel
#UConn
#Email: keshav.patel@uconn.edu
import math

def uCoulToCoul(charge):
    return charge*1e-6

def cmToM(length):
    return length/100

def CoulLaw(q1,q2,x):
    return (k*q1*q2)/x**2
    
def CoulTouCoul(charge):
    return charge/1e-6

if __name__ == '__main__':

    #PART 1    
    
    #Point charges in uC:
    #(I just used my numbers as default values.)
    base_q1 = -2.5
    base_q2 = 7.4

    #Distance, in cm:
    base_x = 9.4

    #constant k:
    k = 9e9

    #convert units so our ans comes out in Newtons
    q1 = uCoulToCoul(base_q1)
    q2 = uCoulToCoul(base_q2)
    x = cmToM(base_x)

    #ques 1
    #coulumb's law to calc the force of q1 on q2.
    print("1)",CoulLaw(q1,q2,x))

    #ques 2
    #This one is slightly more complicated. You need to figure out the angle.
    #you have two sides of a triangle though, so you should be fine.
    
    #a y displacement, in cm
    base_y = 3.8
    y = cmToM(base_y)
    
    #find the angle
    tan_theta = y/x
    theta = math.atan(tan_theta)
    
    #the new distance between 1 and 2 after displacement
    d = ((x**2)+(y**2))**(1/2)

    #the force of charge one on charge two
    force_1_2 = CoulLaw(q1,q2,d)
    
    #Get the x component. 
    force_1_2_x = force_1_2 * math.cos(theta)

    print("2)", force_1_2_x)

    #ques 3 
    #The goal is to find the value of q3, the third particle's charge. 

    #Net force on q2, in Newtons:
    F_net_q2 = 5.4
    
    #The diffrence between the old net force and the new net force will give us the 
    #change in force due to the addition of the new particle.
    delta_F_q2 = force_1_2 - F_net_q2
    
    q3 = (delta_F_q2 * ((d/2)**2))/(k*q2)

    print("3)", -1*CoulTouCoul(q3))
    print("Note: Not sure if I have the vector direction right.\n Try pos/neg if it's wrong")


    #PART 2

    #ques 1
    #New vals for q1, q2, q3:
    base_q1 = 7.3
    base_q2 = -3.6
    base_q3 = 3.5
    
    #the distance d, in cm:
    base_d = 9.4

    q1 = uCoulToCoul(base_q1)
    q2 = uCoulToCoul(base_q2)
    q3 = uCoulToCoul(base_q3)
    d = cmToM(base_d)

    force_1_3 = CoulLaw(q1,q3,d)
    force_2_3 = CoulLaw(q2,q3,d)

    theta = math.radians(60)
    
    #quest 1 & 2
    force_1_3_y = force_1_3*math.sin(theta)
    force_2_3_y = force_2_3*math.sin(theta)

    force_1_3_x = force_1_3*math.cos(theta)
    force_2_3_x = force_2_3*math.cos(theta)
    
    print("1)",(force_1_3_x-force_2_3_x))

    print("note: it could also be",(force_1_3_x+force_2_3_x),"(manage your vectors)")

    print("2)",(force_1_3_y+force_2_3_y))
    print("note: it could also be",(force_1_3_y-force_2_3_y),"(manage your vectors)")

    #quest 3
    #The new introduced charge. It was the same for me, it may be diffrent for you.
    q4 = uCoulToCoul(base_q3)
    
    force_3_2 = CoulLaw(q3, q2, d)
    force_3_2_x = force_3_2 * math.cos(theta)
    
    #note that force_1_2 == force_1_2_x
    force_1_2_x = CoulLaw(q1, q2, d)

    F_net_x_2 = (2*force_3_2_x)+force_1_2_x

    print("3)",F_net_x_2,"\n4) 0")

    #quest 4
    force_2_1_x = -1*force_1_2_x

    force_3_1 = CoulLaw(q3, q1, d)
    force_3_1_x = force_3_1 * math.cos(theta)
 
    F_net_1_x =  force_2_1_x - (2*force_3_1_x) 

    print("5)", F_net_1_x, "Play with the rounding a bit if it doesn't work right. Do feel free to report issues.")













